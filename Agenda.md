
2021 Course
===========

Session 1: Data Analysis and time-series
----------------------------------------

1. Introduction and presentations
2. Python programming language and structure of the course
3. First exercises pandas: Facebook advertising data
4. IoT: predicting RUL for an electric motor from sensors data

Session 2: Learned models
-------------------------

1. Linear Regressor
2. Intermezzo: If you need ROOT
3. Logistic Regressor
4. Introduction to ML
5. If time remains: pytest and TDD

Session 3: Introduction to TensorFlow
-------------------------------------

1. Tensorflow: predictions on images
2. Concluding remarks and thank you

